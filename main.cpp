#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <boost/algorithm/string/trim.hpp>
#include <optional>

// ������ ��������� ������� ��� �������� ���� �� ����� � ���� ��������� ���, ����������, �����
// 
//������� ������� ���-�� ���� setPair, ���� �� ������ ���������� ������ ���� �� ����� � ��������� � ��������, 
//���� ����� ���� ����, �� ������������� ��� ��������. ���� ������ ���� - ��������� ����� ����

//� �������� �� ������ ���� �������\�����

using namespace std;

enum class Status {
	File_access_OK,
	File_access_error,
};

string PrintError(Status status) {
	if (status == Status::File_access_error) {
		return string("File access error");
	}
}

class PropertyTree {
public:	
	/*optional< string > PrintStatus(Status status) {
		if (static_cast<bool>(status)) {
			return status_.value() = "File access error...";
		}
	}*/

	Status serialization() {
		data.clear();

		file.open(file_name, ios::in);
		if (!file.is_open()) {
			return Status::File_access_error;
		}

		while (!file.eof()) {
			pair<string, string> p = getPair();
			data.push_back(p);
		}

		file.close();

		for (size_t i = 0; i < data.size(); i++) {
			if (data[i].first.empty() || data[i].second.empty()) {
				data.erase(data.begin() + i);
			}
		}

		return Status::File_access_OK;
	}
	
	string getValue(string key) {
		for (int i = 0; i < data.size(); i++) {
			if (data[i].first == key) {
				return data[i].second;
			}
		}

		return string("error");
	}
	
	Status deserialization() {
		file.open(file_name, ios::out);
		if (!file.is_open()) {
			return Status::File_access_error;
		}

		for (int i = 0; i < data.size(); i++) {
			file << data[i].first << " = " << data[i].second;
			if (i != data.size() - 1) {
				file << endl;
			}
		}

		file.close();
		return Status::File_access_OK;
	}

	void changeKey(int i, string key) {
		data[i].first = key;
	}

	void changeValue(int i, string value) {
		data[i].second = value;
	}
	
	void addNewKey(string key, string value) {
		pair<string, string> p;

		p.first = key;
		p.second = value;

		data.push_back(p);
	}
	
	void writeQuestions() {
		string key;
		string answer;
		string value;

		cout << "Enter the key which you wanna change or add: ";
		cin >> key;

		bool dont_need_to_add_key = false;

		for (int i = 0; i < data.size(); i++) {
			if (data[i].first == key) {
				cout << "Key change... choose option k - to change key name, or v - to change key value: ";
				cin >> answer;

				if (answer == "k") {
					changeKey(i, key);
				}
				else if (answer == "v") {
					cout << "Enter a new value: ";
					cin >> value;
					changeValue(i, value);
				}

				dont_need_to_add_key = true;
			}
		}

		if (!dont_need_to_add_key) {
			cout << "Key already exist... Adding a new key..." << endl;
			cout << "Enter a key value: ";

			cin >> value;
			addNewKey(key, value);
		}

	}

private:
	pair<string, string> getPair() {

		if (!file.is_open()) {
			cout << "File cannot be opened: " << endl;
			//exit(EXIT_FAILURE);
		}

		pair<string, string> p;

		file >> p.first; // key

		char buffer[128];
		file >> buffer; //space

		file.getline(buffer, 128);// value
		p.second = buffer;
		boost::trim(p.second); // cut

		return p;
	}

	fstream file;
	vector<pair<string, string>> data;
	string file_name = "test8.ini";
	//optional< string > status_;
};

int main() {
	PropertyTree file;
	
	while (true) {
		
		if (file.serialization() == Status::File_access_error) {
			cout << PrintError(Status::File_access_error);
			break;
		}
		
		string answer;
		cout << "Choose option write or read file... enter w - to write, r - to read" << endl;
		cin >> answer;

		if (answer == "w") {
			file.writeQuestions();
		}
		else if (answer == "r") {
			string key;
			cout << "Enter the key: ";
			cin >> key;
			cout << key << " = " << file.getValue(key) << endl;
		}
		else {
			cout << "Try to enter a correct option..." << endl;
		}

		if (file.deserialization() == Status::File_access_error) {
			cout << PrintError(Status::File_access_error);
			break;
		}
	}

	return 0;
}